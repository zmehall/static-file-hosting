const express = require('express')
const app = express()
const multer = require('multer')
const bodyParser = require('body-parser')
const fs = require('fs')

app.use(bodyParser.urlencoded({ extended: true }))
const upload = multer({ dest: '/var/www-static' })
const privateUpload = multer({ dest: '/var/www-static/private' })
app.listen(8081)

app.get('/', (req, res) => {
    res.sendFile('index.html', {root: __dirname})
})

app.get('/upload', (req, res) => {
    res.redirect('https://static.zmehall.dev/')
})

app.get('/delete', (req, res) => {
    res.redirect('https://static.zmehall.dev/')
})

app.get('/ls', (req, res) => {
    let ret = ""
    fs.readdirSync('/var/www-static/').forEach(file => {
	    if(fs.lstatSync('/var/www-static/'+file).isDirectory()) return
      ret += "https://static.zmehall.dev/" + file + "|"
    });
    res.send(ret.substring(0,ret.length-1))
})


app.get('/lsprivate', (req, res) => {
    let ret = ""
    fs.readdirSync('/var/www-static/private/').forEach(file => {
	    if(fs.lstatSync('/var/www-static/private/'+file).isDirectory()) return
      ret += "https://static.zmehall.dev/private/" + file + "|"
    });
    res.send(ret.substring(0,ret.length-1))
})

app.post('/delete', async (req, res) => {
	console.log(req.body)
    if(!req.body.file) return res.redirect("https://static.zmehall.dev")
    const filename = req.body.file.replace('https://static.zmehall.dev/', '').replace('static.zmehall.dev/', '')
    if(fs.existsSync("/var/www-static/" + filename) && !fs.lstatSync('/var/www-static/'+filename).isDirectory()) {
        await fs.unlink("/var/www-static/"+filename, () => {})
    }
    return res.redirect("https://static.zmehall.dev")
})

app.get('/')

app.post('/upload', upload.single('file'), async (req, res) => {
    if(!req.body) return res.redirect('https://static.zmehall.dev/')
    if(req.file) {
        let filename = await getUniqueRename('/var/www-static/', removeTags(req.file.originalname))
        fs.renameSync(req.file.path, "/var/www-static/" + filename)
        res.redirect('https://static.zmehall.dev/' + filename)
        return
    }
    res.redirect('https://static.zmehall.dev/')
})


app.post('/uploadprivate', privateUpload.single('file'), async (req, res) => {
    if(!req.body) return res.redirect('https://static.zmehall.dev/')
    if(req.file) {
        let filename = await getUniqueRename('/var/www-static/private/', removeTags(req.file.originalname))
        fs.renameSync(req.file.path, "/var/www-static/private/" + filename)
        res.redirect('https://static.zmehall.dev/private/' + filename)
        return
    }
    res.redirect('https://static.zmehall.dev/')
})

// returns name of valid file
async function getUniqueRename(basedir, name) {
    let num = 0;
    if(!fs.existsSync(basedir + name)) return name
    while(fs.existsSync(basedir + name)) {
        const index = name.lastIndexOf('.')
        if(index > -1) {
            if(num == 0) {
                name = name.substring(0, index - (num.toString().length)+1) + num.toString() + name.substring(index)
            } else {
                name = name.substring(0, index - ((num-1).toString().length)) + num.toString() + name.substring(index)
            }
        } else {
            if(num == 0) {
                name = name.substring(0, index - (num.toString().length)-1) + num.toString()
            } else {
                name = name.substring(0, index - ((num-1).toString().length)) + num.toString()
            }
        }
        num++
    }
    return name
}

function removeTags(str) {
    return str.replace(/</g, "").replace(/>/g, "").replace(/\//g, "")
}
